import React from 'react';
import './App.scss';
import DeckGlView from './components/deck-gl-view/deck-gl-view';
import {
  BrowserRouter as Router,
  Route,
} from "react-router-dom";
import TilesetData from './components/tileset-data/tileset-data';

function App() {
  return (
    <Router>
      <Route exact path="/">
        <DeckGlView />
      </Route>
      <Route exact path="/i3s">
        <DeckGlView loader="i3s" />
      </Route>
      <Route exact path="/tileset-data">
        <TilesetData />
      </Route>
    </Router>
  );
}

export default App;
