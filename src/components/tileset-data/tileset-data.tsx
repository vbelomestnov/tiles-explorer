import React, { useState, useEffect } from 'react';
import { Treebeard } from 'react-treebeard';
import './tileset-data.scss';

function TilesetData() {
    const [tilesetData, setTilesetData] = useState({ name: '...Loading', loading: true });
    const [cursor, setCursor] = useState({ active: true });
    const [selectedNode, setSelectedNode] = useState();

    useEffect(() => {
        fetch('http://localhost/tileset-parse')
            .then(res => res.json())
            .then((data: any) => {
                setTilesetData(data);
            });
    }, []);

    const onToggle = (node: any, toggled: any) => {
        if (cursor) {
            cursor.active = false;
        }
        node.active = true;
        if (node.children) {
            node.toggled = toggled;
        }
        setCursor(node);
        setTilesetData(Object.assign({}, tilesetData));
        setSelectedNode(node);
    }

    const _renderDescription = () => {
        if (!selectedNode) {
            return 'None';
        } else {
            return (
                <dl>
                    <dt>Name</dt>
                    <dd>{selectedNode.name}</dd>
                    <dt>Type</dt>
                    <dd>{selectedNode.type}</dd>
                    <dt>Size (bytes)</dt>
                    <dd>{selectedNode.size}</dd>
                    <dt>Point Count</dt>
                    <dd>{selectedNode.pointCount}</dd>
                </dl>
            );
        }
    }

    return (
        <div className="layout">
            <div className="tree-view">
                <Treebeard data={tilesetData} onToggle={onToggle} />
            </div>
            <div className="description">
                {_renderDescription()}
            </div>
        </div>
    );
}

export default TilesetData;