import React, { useState } from 'react';
import { StaticMap, FlyToInterpolator, ViewportProps } from 'react-map-gl';
import DeckGL from '@deck.gl/react';
import { registerLoaders } from '@loaders.gl/core';
import { CesiumIonLoader } from '@loaders.gl/3d-tiles';
import { I3SLoader } from '@loaders.gl/i3s';
import { Tile3DLayer } from '@deck.gl/geo-layers';
import { Tileset } from './interfaces/tileset.interface';
import { DracoWorkerLoader } from '@loaders.gl/draco';


const MAPBOX_TOKEN = process.env.MapboxAccessToken || 'pk.eyJ1IjoiYmVsb204OCIsImEiOiJjazkwY2FobWUwMDl4M2ZuNWY4eThjanJ6In0.Gz9I67OqnaU23ayzEKURYg'; // eslint-disable-line
const ION_TOKEN = process.env.IonToken || 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI4ZTZlMjI3MC1lYmI0LTRmMjctODU0Mi0yN2ZiNmYxY2VhODkiLCJpZCI6MjYxMzMsInNjb3BlcyI6WyJhc2wiLCJhc3IiLCJhc3ciLCJnYyIsInByIl0sImlhdCI6MTU4NzMzMjQ4Nn0.IeeAR9E0BXo3hMN7JhisCrE8qES7Sx_fuvmD4UXoLJo'; // eslint-disable-line

const TRANSITION_DURAITON = 4000;
const INITIAL_VIEW_STATE: ViewportProps = {
    latitude: 0,
    longitude: 0,
    zoom: 12,
    maxZoom: 16,
    pitch: 60,
    bearing: 0,
    width: 0,
    height: 0,
    altitude: 0,
    minPitch: 0,
    maxPitch: 90,
    minZoom: 0,
};

function DeckGlView(props: { loader?: string }) {
    const [viewState, setViewState] = useState(INITIAL_VIEW_STATE);

    function _renderLayersCesium() {
        registerLoaders([DracoWorkerLoader]);
        return [new Tile3DLayer({
            id: 'tile-3d-layer',
            data: 'http://localhost/3d-tiles/tileset.json',
            loader: CesiumIonLoader,
            loadOptions: {
                'cesium-ion': { accessToken: ION_TOKEN },
            },
            pickable: true,
            pointSize: 1,
            onTilesetLoad: _onTilesetLoad,
        })];
    }

    function _renderLayersI3s() {
        const loadOptions = { throttleRequests: true };
        return [
            new Tile3DLayer({
                data: 'http://localhost:3001/i3s/SceneServer/layers/0',
                loader: I3SLoader,
                onTilesetLoad: _onTilesetLoad,
                loadOptions
            })
        ];
    }

    function _onTilesetLoad(tileset: Tileset) {
        const { cartographicCenter, zoom } = tileset;
        const [longitude, latitude] = cartographicCenter;

        const newViewState = {
            ...viewState,
            zoom,
            longitude,
            latitude
        };
        setViewState({
            ...newViewState,
            transitionDuration: TRANSITION_DURAITON,
            transitionInterpolator: new FlyToInterpolator()
        });
    }

    const { loader } = props;
    let renderLayersFunc = _renderLayersCesium;
    if (loader === 'i3s') {
        renderLayersFunc = _renderLayersI3s;
    }
    return (
        <DeckGL
            layers={renderLayersFunc()}
            initialViewState={viewState}
            controller={true}
        >
            <StaticMap
                reuseMaps
                mapStyle={'mapbox://styles/mapbox/dark-v9'}
                preventStyleDiffing={true}
                mapboxApiAccessToken={MAPBOX_TOKEN}
                width="100%"
                height="100%"
            />
        </DeckGL>
    );
}

export default DeckGlView;