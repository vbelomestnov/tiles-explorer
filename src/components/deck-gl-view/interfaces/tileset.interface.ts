export interface Tileset {
    zoom: number;
    cartographicCenter: number[];
}